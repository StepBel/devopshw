FROM python:3.9-slim

WORKDIR /opt

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN apt-get update && \
    apt-get install -y curl git libpq-dev gcc postgresql postgresql-contrib netcat 
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY . .

RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]

CMD ["python", "manage.py", "runserver", "8000"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 --start-period=5s \
    CMD curl -f 127.0.0.1:8000/api/example/v1/healthcheck/ || exit 1
