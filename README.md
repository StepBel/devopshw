## Установка

Чтобы все работало, нужно иметь

- python 3.9+
- libpq-dev

---

1. Установите зависимости

   ```bash
   pip3 install -r requirements.txt
   ```

2. Запустите локальный psql-сервер. Возможно, вам поможет [это](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04-ru). Достаточно дойти до шага у успешным запуском

   ```bash
   psql
   ```

3. Нужно создать бдшки, описанные в **DATABASES** в _cloud_pc/settings.py_. 

Для этого создайте `.env` файл в корне проекта. Запишите в него

* USER - имя роли
* NAME - имя БД
* PASSWORD - пароль от роли
* SECRET_KEY - рандомная строка из 50 символов

Пример (можно просто скопировать): 

   ```bash
   DEBUG=1
   NAME=example
   USER=postgres_admin
   PASSWORD=qwertyparol
   SECRET_KEY='django-insecure-hzlx721%05cw^+gb(_s)gtdh+9u!lpl00ow#&22k53^%%b71_f'
   ```

Выполните `export $(cat .env | xargs)`, чтобы загрузить переменные окружения. Затем запустите скрипт создания роли и БД:

   ```bash
   scripts/create_db.sh
   ```

5. Нужно запустить миграции

   ```bash
   ./manage.py makemigrations
   ./manage.py migrate
   ```

6. Если все хорошо, то тесты должны уметь запускаться и выдавать красивый зеленый ОК

   ```bash
   ./manage.py test
   ```

7. Нужно установить pre-commit хуки, чтобы при ваших коммитах проверялся кодстайл:

   ```bash
   pre-commit install
   ```

   При `git commit ...` они автоматиески проверят внесенные изменения и отредактируют некрасивый код. Можно явно запустить их без коммита:

   - только на поменявшихся с последнего коммита файлах:
     ```bash
     pre-commit run
     ```
   - на вообще всех файлах
     ```bash
     pre-commit run --all-files
     ```

---

### Локальный запуск

Если хочется собрать у себя бэк, можно это сделать через

```bash
./manage.py runserver 8000
```

Теперь в него можно стучаться в `http://127.0.0.1:8000` и в `http://localhost:8000`


### Systemd

```
[Unit]
Description=schedulers_hw
After=syslog.target

[Service]
Type=forking

User=sample_user
Group=sampe_user

ExecStart=/usr/local/bin/python3.10 {path_to_project}/manage.py runserver 8000
TimeoutSec=300
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```
#### Проверка работоспособности
```
sudo systemctl enable {service}
sudo systemctl start {service}
sudo systemctl status {service}
```

### [openApi](adminpage_OPENAPI.yaml)


