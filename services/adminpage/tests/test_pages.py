import random
import uuid
from unittest import skip

from django.test import LiveServerTestCase
from selenium import webdriver


@skip('Run on specific OS')
class TestPages(LiveServerTestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(
            executable_path='services/adminpage/tests/static/geckodriver')

    def tearDown(self):
        self.driver.close()

    def test_page_title(self):
        self.driver.get(self.live_server_url + '/adminpage/')
        self.assertEqual('adminpage', self.driver.title)

    def test_users_button(self):
        self.driver.get(self.live_server_url + '/adminpage/')
        user_button = self.driver.find_element('id', 'users_button_in_header')
        user_button.click()

    def test_add_and_find_user(self):
        self.driver.get(self.live_server_url + '/adminpage/')
        user_button = self.driver.find_element('id', 'users_button_in_header')
        user_button.click()

        find_email_test = ''

        user_cnt = 20
        copy_num = random.randint(0, user_cnt-1)
        for i in range(user_cnt):
            email_input = self.driver.find_element('id', 'id_email')
            email_input.clear()
            email_name = 'some_new_email' + str(uuid.uuid4())[:8] + '@email.test'
            email_input.send_keys(email_name)
            if i == copy_num:
                find_email_test = (email_name + '.')[:-1]

            balance_input = self.driver.find_element('id', 'id_balance')
            balance_input.clear()
            balance_input.send_keys(random.randint(0, 10000000))

            password_input = self.driver.find_element('id', 'id_password')
            password_input.clear()
            password_input.send_keys(str(uuid.uuid4())[:8])

            self.driver.find_element('id', 'id_add_user').click()

        find_by_email = self.driver.find_element('id', 'myInput')
        find_by_email.clear()
        find_by_email.send_keys(find_email_test)

        table = self.driver.find_element('xpath', '//table[@id="users"]/tbody')
        self.assertEqual(table.text.partition(' ')[0], find_email_test)
