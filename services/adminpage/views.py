from django.shortcuts import redirect, render

from ..computers.models import Computer
from ..users.models import User
from .forms import ChangeUserForm


def main_page(request):
    users = User.objects.all()
    computers = Computer.objects.all()
    total_memory = 0
    for computer in computers:
        total_memory += computer.external_memory

    cards = [{'title': 'Users registered', 'name': 'users', 'amount': users.count},
             {'title': 'Computers in use', 'name': '#', 'amount': computers.count},
             {'title': 'total minutes used', 'name': '#', 'amount': 'who knows'},
             {'title': 'total disk usage', 'name': '#', 'amount': total_memory},
             {'title': 'popolneniya ', 'name': '#', 'amount': 'who knows'},
             {'title': 'chtoto estche', 'name': '#', 'amount': 'who knows'},
             ]

    context = {
        'cards': cards,
    }
    return render(request, 'adminpage/main_page.html', context)


def userlist(request):
    if request.method == 'POST':
        form = ChangeUserForm(request.POST)
        if form.is_valid():

            data = form.cleaned_data
            if User.objects.all().filter(email=data['email']) or data['new_password'] is None:
                print('this email exists or password is empty')
                # TODO
                # render that issue on page
                return redirect('userspage')

            new_user = User.objects.create(
                username=data['email'], email=data['email'], balance=data['balance'])
            new_user.set_password(data['new_password'])
            return redirect('userspage')
        else:
            print(form.errors)
            new_user_form = ChangeUserForm()
            # TODO
    else:
        new_user_form = ChangeUserForm()
    context = {
        'users': User.objects.all(),
        'new_user_form': new_user_form,
    }
    return render(request, 'adminpage/userlist.html', context)


def userpage(request, pk):
    context = {
        'user': User.objects.get(pk=pk)
    }
    return render(request, 'adminpage/user.html', context)


def change_user(request, pk):
    user = User.objects.get(pk=pk)
    previous_data = {
        'email': user.email,
        'balance': user.balance,
    }
    if request.method == 'POST':
        form = ChangeUserForm(request.POST, initial=previous_data)
        if form.is_valid():
            new_data = form.cleaned_data
            if new_data['email'] != user.email \
                    and User.objects.all().filter(email=new_data['email']):
                print('this email exists')
                # TODO
                # render that issue on page
                return redirect('userspage')

            user.email = new_data['email']
            user.username = new_data['email']
            user.balance = new_data['balance']
            if new_data['new_password'] is not None:
                user.set_password(new_data['new_password'])
            user.save()
            return redirect('userspage')
    else:
        form = ChangeUserForm(initial=previous_data)

    context = {
        'user': user,
        'form': form,
    }
    return render(request, 'adminpage/change_user.html', context)
