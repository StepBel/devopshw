from django.urls import path

from .views import change_user, main_page, userlist, userpage

urlpatterns = [
    path('', main_page, name='adminhome'),
    path('users/', userlist, name='userspage'),
    path('users/<str:pk>', userpage),
    path('users/change/<str:pk>', change_user),
]
