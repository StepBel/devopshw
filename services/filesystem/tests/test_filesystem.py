# import os
#
# from django.core.files.uploadedfile import SimpleUploadedFile
# from django.test import TestCase
#
# from ..sftp.metadata import FileType
# from ..sftp.remote_filesystem import RemoteFilesystem
#
#
# class TestSFTP(TestCase):
#     test_folder = 'C:/test_paramiko'
#     sub_filename = os.path.join(test_folder, 'sub_filename')
#     sub_dirname = os.path.join(test_folder, 'sub_dirname')
#     sub_filename_content = b'Hello, world!\n'
#
#     @staticmethod
#     def _set_remote_fs():
#         return RemoteFilesystem()
#
#     def setup(self):
#         fs = self._set_remote_fs()
#
#         try:
#             fs.get_meta(self.test_folder)
#             fs.remove(self.test_folder)
#         except FileNotFoundError:
#             pass
#
#         fs.makedir(self.test_folder)
#
#         with fs.open(self.sub_filename, 'w') as file:
#             file.write(self.sub_filename_content)
#
#         fs.makedir(self.sub_dirname)
#
#     def cleanup(self):
#         fs = self._set_remote_fs()
#         fs.remove(self.test_folder)
#
#     def test_setup(self):
#         self.setup()
#
#         fs = self._set_remote_fs()
#
#         files = fs.listdir(self.test_folder)
#         self.assertEqual(len(files), 2)
#
#         for meta in files:
#             if meta.filename == self.sub_filename:
#                 self.assertEqual(meta.filetype, FileType.FILE)
#             if meta.filename == self.sub_dirname:
#                 self.assertEqual(meta.filetype, FileType.DIR)
#
#         with fs.open(self.sub_filename, 'r') as file:
#             self.assertEqual(file.read(), self.sub_filename_content)
#
#         self.cleanup()
#
#     def test_listdir(self):
#         self.setup()
#         response = self.client.get('/api/filesystem/v1/files_list/', data={
#             'prefix': self.test_folder,
#             'max_files': 1024,
#         })
#
#         self.assertTrue(response.status_code, 200)
#
#         self.assertTrue('files' in response.data)
#         files = response.data['files']
#         self.assertEqual(len(files), 2)
#         for file in files:
#             self.assertTrue('path' in file)
#             self.assertTrue('size' in file)
#             self.assertTrue('file_type' in file)
#             if file['path'] == self.sub_filename:
#                 self.assertEqual(file['size'], len(self.sub_filename_content))
#                 self.assertEqual(file['file_type'], 'FILE')
#             else:
#                 self.assertEqual(file['file_type'], 'DIR')
#
#         self.cleanup()
#
#     def test_get_file(self):
#         self.setup()
#         response = self.client.get('/api/filesystem/v1/file/', data={
#             'path': self.sub_filename,
#         })
#         self.assertEqual(response.status_code, 200)
#
#         self.assertTrue('path' in response.data)
#         self.assertEqual(response.data['path'], self.sub_filename)
#
#         self.assertTrue('file_type' in response.data)
#         self.assertEqual(response.data['file_type'], 'FILE')
#
#         self.assertTrue('size' in response.data)
#         self.assertEqual(response.data['size'], len(self.sub_filename_content))
#
#         self.cleanup()
#
#     def test_post_file(self):
#         self.setup()
#
#         new_test_context = b'Hello world!!!!!'
#
#         simple_file = SimpleUploadedFile('file.mp4', new_test_context)
#
#         response = self.client.post('/api/filesystem/v1/file/', data={
#             'path': self.sub_filename,
#             'content': simple_file,
#         })
#         self.assertEqual(response.status_code, 200)
#
#         fs = self._set_remote_fs()
#
#         with fs.open(self.sub_filename, 'rb') as file:
#             self.assertEqual(new_test_context, file.read())
#
#         self.cleanup()
#
#     def test_delete_file(self):
#         self.setup()
#
#         response = self.client.delete('/api/filesystem/v1/file/', data={
#             'path': self.sub_filename,
#         })
#         self.assertEqual(response.status_code, 200)
#
#         fs = self._set_remote_fs()
#
#         self.assertFalse(fs.isfile(self.sub_filename))
#
#         self.cleanup()
#
#     def test_patch_file(self):
#         self.setup()
#
#         sub_filename_2 = os.path.join(self.sub_dirname, 'sub_filename_2')
#
#         response = self.client.post('/api/filesystem/v1/rename/', data={
#             'old_path': self.sub_filename,
#             'new_path': sub_filename_2,
#         })
#
#         self.assertTrue(response.status_code, 200)
#
#         fs = self._set_remote_fs()
#
#         self.assertFalse(fs.isfile(self.sub_filename))
#         self.assertTrue(fs.isfile(sub_filename_2))
#
#         with fs.open(sub_filename_2, 'rb') as file:
#             self.assertEqual(file.read(), self.sub_filename_content)
#
#         self.cleanup()
