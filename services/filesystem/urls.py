from django.urls import path

from . import views

urlpatterns = [
    path('v1/file/', views.FileDetailView.as_view(), name='operations with file'),
    path('v1/rename/', views.FileRenameView.as_view(), name='operations with file'),
    path('v1/files_list/', views.FileListView.as_view(), name='file list'),
]
