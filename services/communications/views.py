import os

from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import get_object_or_404
from django.template import loader

from services.users.models import User


def custom_send_email(user_pk, email_type, context):

    person = get_object_or_404(User, pk=user_pk)

    templates_path = os.path.join(*[settings.BASE_DIR, 'templates', 'email', email_type])
    subject_path = f'{templates_path}_subject.txt'
    message_path = f'{templates_path}_message.txt'

    subject = loader.render_to_string(subject_path, context)
    subject = subject.replace('\n', '')

    message = loader.render_to_string(message_path, context)

    message = EmailMessage(
        subject=subject,
        to=[person.email, ],
        from_email=settings.DEFAULT_FROM_EMAIL,
        body=message,
    )

    message.content_subtype = 'html'
    try:
        message.send()
    except Exception:
        message.error('Unable to send email.')
