from django.contrib import admin

from .models import Tokens, User


class Admin(admin.ModelAdmin):
    model = User
    list_display = ['email', 'username', 'id']


admin.site.register(User, Admin)
admin.site.register(Tokens)
