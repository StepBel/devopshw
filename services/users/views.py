from django.contrib.auth import login
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from rest_auth.registration.views import RegisterView, VerifyEmailView
from rest_auth.views import PasswordChangeView
from rest_framework import generics

from . import models, serializers

sensitive_post_parameters_m = method_decorator(
    sensitive_post_parameters(
        'new_password1'
    )
)


class UserListView(generics.ListCreateAPIView):
    """
    List all users, or create a new user.
    """
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a user instance.
    """
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer


class CustomRegisterView(RegisterView):
    serializer_class = serializers.CustomRegisterSerializer


class CustomPasswordChangeView(PasswordChangeView):
    serializer_class = serializers.CustomPasswordChangeSerializer

    @sensitive_post_parameters_m
    def dispatch(self, *args, **kwargs):
        return super(PasswordChangeView, self).dispatch(*args, **kwargs)


class CustomVerifyEmailView(VerifyEmailView):

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.kwargs['key'] = serializer.validated_data['key']
        confirmation = self.get_object()
        confirmation.confirm(self.request)

        user = models.User._default_manager.get(email=confirmation.email_address)
        login(request, user)
        url = reverse('rest_custom_password_change')
        return HttpResponseRedirect(url)
