import json

from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.test import TestCase
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from services.users.models import User


class TestResetConfirm(TestCase):
    def setUp(self):
        User.objects.create(
            username='Misha',
            email='some_mail@yandex.ru',
            password='passwordmisha123'
        )
        self.client.post('/api/auth/reset/', {'email': 'some_mail@yandex.ru'})

    def test_reset_confirm(self):
        email = 'some_mail@yandex.ru'
        active_users = get_user_model()._default_manager.filter(
            email__iexact=email, is_active=True)
        users = [u for u in active_users if u.has_usable_password()]

        self.assertEqual(len(users), 1)
        user = users[0]

        token = default_token_generator.make_token(user)
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        response = self.client.post(f'/api/auth/confirmation/{uid}/{token}/', {
            'new_password1': 'newpasswordtest27',
            'new_password2': 'newpasswordtest27',  # TODO remove redundant
            'token': str(token),
            'uid': str(uid),
        })
        with open('services/users/tests/static/test_reset_confirm/expected_response.json') as f:
            expected_response = json.load(f)
            self.assertEqual(json.loads(response.content), expected_response)
