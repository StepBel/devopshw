import json

from django.test import TestCase

from services.users.models import User


class TestUsersList(TestCase):
    def setUp(self):
        User.objects.create(
            username='Misha',
            email='some_mail@yandex.ru'
        )

    def test_reset_password(self):
        response = self.client.post('/api/auth/reset/', {'email': 'some_mail@yandex.ru'})
        with open('services/users/tests/static/test_reset_password/expected_response.json') as f:
            expected_response = json.load(f)
            self.assertEqual(json.loads(response.content), expected_response)
