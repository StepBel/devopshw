import json

from django.test import TestCase

from services.users.models import User


class TestLogin(TestCase):
    def setUp(self):

        user = User.objects.create(
            username='Misha',
            email='dragun.kiu@phystech.edu'
        )
        user.set_password('qwertypassword')
        user.save()

    def test_login(self):
        response = self.client.post('/api/auth/login/', {
            'email': 'dragun.kiu@phystech.edu',
            'password': 'qwertypassword',
        })

        key = json.loads(response.content)
        self.assertEqual([k for k in key], ['key'])

        response = self.client.post('/api/auth/logout/')

        with open('services/users/tests/static/test_login/expected_response.json') as f:
            expected_response = json.load(f)
            self.assertEqual(json.loads(response.content), expected_response)
