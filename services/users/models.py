from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    email = models.EmailField(
        'email address',
        max_length=150,
        unique=True,
        error_messages={
            'unique': 'A user with that email already exists.',
        },
    )
    balance = models.IntegerField(default=0)
    name = models.CharField(blank=True, max_length=255)

    def __str__(self):
        return self.email

    def __unicode__(self):
        return self.id


class Tokens(models.Model):
    email = models.EmailField(blank=True)
    time = models.DurationField()

    def __str__(self):
        time_str = from_timedelta(self.time).strftime('%m/%d/%Y, %H:%M:%S')
        return f'Email: {self.email}; Time: {time_str} GMT+3'


def to_timedelta(time: datetime = datetime.now()) -> timedelta:
    return time - datetime(2001, 1, 1)


def from_timedelta(delta: timedelta) -> datetime:
    return delta + datetime(2001, 1, 1)


def truncate_sent_counter():
    time_bound = to_timedelta() - settings.TOKEN_LIFETIME
    Tokens.objects.filter(time__lt=time_bound).delete()
