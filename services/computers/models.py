from django.core.validators import MinValueValidator
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy


class Computer(models.Model):
    name = models.TextField(null=True)
    owner = models.ForeignKey(
        'users.User',
        blank=True,  # чтобы валидатор разрешал null
        null=True,  # чтобы можно было хранить null :)
        on_delete=models.SET_NULL,
    )
    cloud_id = models.TextField(unique=True)

    @cached_property
    def status(self):
        return Status.objects.filter(computer_id=self.id).latest()

    @cached_property
    def configuration(self):
        return Configuration.objects.filter(computer_id=self.id).latest()

    class Meta:
        indexes = [
            models.Index(fields=['owner']),
            models.Index(fields=['cloud_id'])
        ]


class Status(models.Model):
    class StatusValue(models.TextChoices):
        kOn = 'ON', gettext_lazy('Computer is turned on')
        kOff = 'OFF', gettext_lazy('Computer is turned off')
        kTurningOn = 'TURNING_ON', gettext_lazy('Computer is turning on')
        kTurningOff = 'TURNING_OFF', gettext_lazy('Computer is turning off')
        kChangingConfiguration = 'CHANGING_CONFIGURATION', \
            gettext_lazy('Changing configuration')

    computer = models.ForeignKey(
        'Computer',
        on_delete=models.CASCADE
    )
    value = models.CharField(
        max_length=30,
        choices=StatusValue.choices,
        default=StatusValue.kOff
    )
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        get_latest_by = 'date'
        ordering = ['-date']
        indexes = [
            models.Index(fields=['computer']),
            models.Index(fields=['computer', 'date'])
        ]


class Configuration(models.Model):
    computer = models.ForeignKey(
        'Computer',
        on_delete=models.CASCADE
    )
    date = models.DateTimeField(auto_now_add=True)

    datacenter = models.ForeignKey(
        'DataCenter',
        on_delete=models.PROTECT,
        to_field='key'
    )

    os = models.ForeignKey(
        'OperatingSystem',
        on_delete=models.PROTECT,
        to_field='key'
    )

    cpu = models.ForeignKey(
        'CPU',
        on_delete=models.PROTECT,
        to_field='key'
    )
    cpu_cores = models.IntegerField(
        validators=[MinValueValidator(1)]
    )

    ram = models.ForeignKey(
        'RAM',
        on_delete=models.PROTECT,
        to_field='key'
    )
    ram_volume = models.IntegerField(
        validators=[MinValueValidator(1)]
    )

    storage_device = models.ForeignKey(
        'StorageDevice',
        on_delete=models.PROTECT,
        to_field='key'
    )
    storage_device_volume = models.IntegerField(
        validators=[MinValueValidator(1)]
    )

    gpu = models.ForeignKey(
        'GPU',
        on_delete=models.PROTECT,
        to_field='key'
    )

    class Meta:
        get_latest_by = 'date'
        ordering = ['-date']
        indexes = [
            models.Index(fields=['computer'])
        ]


class OperatingSystem(models.Model):
    key = models.TextField(unique=True)
    full_name = models.TextField()


class CPU(models.Model):
    key = models.TextField(unique=True)
    full_name = models.TextField()


class RAM(models.Model):
    key = models.TextField(unique=True)
    full_name = models.TextField()


class GPU(models.Model):
    key = models.TextField(unique=True)
    full_name = models.TextField()


class StorageDevice(models.Model):
    key = models.TextField(unique=True)
    full_name = models.TextField()
    driver_type = models.TextField()


class DataCenter(models.Model):
    key = models.TextField(unique=True)
    name = models.TextField()
    country = models.TextField()
    country_flag_key = models.TextField()
