import json

from django.test import TransactionTestCase
from freezegun import freeze_time

from .. import models
from . import utils

TEST_DIR = 'services/computers/tests/static/test_configurations'


class TestConfigurations(TransactionTestCase):
    maxDiff = None
    reset_sequences = True

    @freeze_time('2022-03-19 12:12:12')
    def setUp(self):
        self.user = utils.common_set_up()

    @freeze_time('2022-03-19 12:12:12')
    def test_patch(self):
        models.OperatingSystem.objects.create(
            key='MACOS_MONTEREY',
            full_name='macOS Monterey'
        )
        self.client.force_login(self.user)
        response = self.client.patch(
            '/api/computers/v1/configuration/1/',
            {
                'os': 'MACOS_MONTEREY',
            },
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/patch_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    def test_configuration_history(self):
        self.client.force_login(self.user)
        for i in range(1, 6):
            with freeze_time(f'2022-03-19 12:12:1{i}'):
                response = self.client.patch(
                    '/api/computers/v1/configuration/1/',
                    {
                        'internal_memory': i
                    },
                    content_type='application/json',
                )
                self.assertEqual(response.status_code, 200)

        response = self.client.get('/api/computers/v1/configuration_history/1/')
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/history_response.json') as f:
            self.assertListEqual(json.loads(response.content), json.load(f))


class TestConfigurationFetching(TransactionTestCase):
    maxDiff = None

    @freeze_time('2022-03-19 12:12:12')
    def setUp(self):
        utils.common_set_up()

    def test_default(self):
        response = self.client.get(
            '/api/computers/v1/configurations_list/',
            {}
        )

        with open(f'{TEST_DIR}/default_configurations_list.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    def test_partial(self):
        response = self.client.get(
            '/api/computers/v1/configurations_list/',
            {
                'os': 'Windows 10',
                'cpu': 12,
                'ram': 24,
                'gpu': 'gpu1',
            }
        )

        with open(f'{TEST_DIR}/partial_configurations_list.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))
