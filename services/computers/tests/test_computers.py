import json

from django.test import TransactionTestCase
from freezegun import freeze_time

from . import utils

TEST_DIR = 'services/computers/tests/static/test_computers'


class TestComputers(TransactionTestCase):
    maxDiff = None
    reset_sequences = True

    @freeze_time('2022-03-19 12:12:12')
    def setUp(self):
        self.user = utils.common_set_up()

    @freeze_time('2022-03-19 12:12:12')
    def test_get(self):
        self.client.force_login(self.user)
        response = self.client.get('/api/computers/v1/computer/1/')
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/get_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    @freeze_time('2022-03-19 12:12:12')
    def test_post(self):
        self.client.force_login(self.user)
        response = self.client.post(
            '/api/computers/v1/computer/',
            {
                'owner': '1',
                'name': 'BomBomich',
                'cloud_id': 'b1gp78ncthsf17ravgac',
                'configuration': {
                    'datacenter': 'ekb',
                    'os': 'WINDOWS10',
                    'cpu': 'Intel_Core_i7_8700T_OEM',
                    'cpu_cores': 2,
                    'ram': 'Corsair_Vengeance_LPX_32GB',
                    'ram_volume': 32,
                    'storage_device': 'SAMSUNG_970_EVO_Plus_SSD_2TB',
                    'storage_device_volume': 2000,
                    'gpu': 'NVIDIA_GeForce_RTX_3060',
                }
            },
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/post_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    @freeze_time('2022-03-19 12:1:12')
    def test_delete(self):
        self.client.force_login(self.user)
        response = self.client.delete('/api/computers/v1/computer/1/')
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/delete_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

        response = self.client.get('/api/computers/v1/computer/1/')
        self.assertEqual(response.status_code, 404)

    def test_patch(self):
        self.client.force_login(self.user)
        response = self.client.patch(
            '/api/computers/v1/computer/1/',
            {
                'name': 'other name'
            },
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        with open(f'{TEST_DIR}/patch_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

    def test_list_computers(self):
        self.client.force_login(self.user)
        response = self.client.get(
            '/api/computers/v1/computers/',
        )
        self.assertEqual(response.status_code, 200)

        with open(f'{TEST_DIR}/bulk_get_response.json') as f:
            self.assertListEqual(json.loads(response.content), json.load(f))
