import json

from django.test import TransactionTestCase
from freezegun import freeze_time

from .. import models
from . import utils


class TestStatuses(TransactionTestCase):
    maxDiff = None
    reset_sequences = True

    def setUp(self):
        self.user = utils.common_set_up()

    @freeze_time('2022-03-19 12:12:12')
    def test_activation(self):
        self.client.force_login(self.user)
        response = self.client.patch(
            '/api/computers/v1/status/1/',
            {
                'value': 'ON'
            },
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 201)
        with open('services/computers/tests/static/test_statuses/activation_response.json') as f:
            self.assertDictEqual(json.loads(response.content), json.load(f))

        latest_status = models.Status.objects.filter(computer_id=1).latest()
        self.assertTrue(latest_status.value == 'ON')

    def test_history(self):
        self.client.force_login(self.user)
        queries = ['ON', 'OFF',
                   'ON', 'ON',
                   'OFF', 'OFF']

        for i, query in enumerate(queries):
            with freeze_time(f'2022-03-19 12:12:1{i}'):
                response = self.client.patch(
                    '/api/computers/v1/status/1/',
                    {
                        'value': query
                    },
                    content_type='application/json',
                )

        response = self.client.get('/api/computers/v1/status_history/1/')
        self.assertEqual(response.status_code, 200)
        with open('services/computers/tests/static/test_statuses/history_response.json') as f:
            self.assertListEqual(json.loads(response.content), json.load(f))
