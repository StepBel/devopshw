from datetime import datetime

import django
from freezegun import freeze_time

from ...users.models import User
from .. import models

django.setup()


@freeze_time('2022-03-18 12:12:12')
def common_set_up():
    user = User(
        username='testuser',
    )
    user.set_password('qwerty')
    user.save()

    computer = models.Computer.objects.create(
        name='My computer',
        owner=user,
        cloud_id='b1gp78ncthsf17ravgaa'
    )

    models.GPU.objects.create(key='gpu1')
    models.GPU.objects.create(key='gpu2')

    datacenter = models.DataCenter.objects.create(
        key='ekb',
        name='Yekaterinburg',
        country='Russia',
        country_flag_key='rus'
    )
    os = models.OperatingSystem.objects.create(
        key='WINDOWS10',
        full_name='Windows 10'
    )
    cpu = models.CPU.objects.create(
        key='Intel_Core_i7_8700T_OEM',
        full_name='Intel Core i7-8700T OEM',
    )
    ram = models.RAM.objects.create(
        key='Corsair_Vengeance_LPX_32GB',
        full_name='Corsair Vengeance LPX 32GB',
    )
    gpu = models.GPU.objects.create(
        key='NVIDIA_GeForce_RTX_3060',
        full_name='MSI Gaming GeForce RTX 3060'
    )
    storage_device = models.StorageDevice.objects.create(
        key='SAMSUNG_970_EVO_Plus_SSD_2TB',
        full_name='SAMSUNG 970 EVO Plus SSD 2TB',
        driver_type='SSD',
    )

    models.Configuration.objects.create(
        computer=computer,
        datacenter=datacenter,
        os=os,
        cpu=cpu,
        cpu_cores=6,
        ram=ram,
        ram_volume=32,
        storage_device=storage_device,
        storage_device_volume=2000,
        gpu=gpu,
    )
    models.Status.objects.create(
        computer=computer,
        value='OFF',
        date=datetime.now()
    )

    other_computer = models.Computer.objects.create(
        name='Computer for playing solitaire',
        owner=user,
        cloud_id='b1gp78ncthsf17ravgab'
    )
    other_storage_device = models.StorageDevice.objects.create(
        key='Seagate_BarraCuda_2TB_Internal_Hard_Drive_HDD',
        full_name='Seagate BarraCuda 2TB Internal Hard Drive HDD',
        driver_type='HDD',
    )
    models.Configuration.objects.create(
        computer=other_computer,
        datacenter=datacenter,
        os=os,
        cpu=cpu,
        cpu_cores=6,
        ram=ram,
        ram_volume=32,
        storage_device=other_storage_device,
        storage_device_volume=2000,
        gpu=gpu,
    )
    models.Status.objects.create(
        computer=other_computer,
        value='TURNS_ON',
        date=datetime.now()
    )

    return user
