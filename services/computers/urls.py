from django.urls import path

from . import views

urlpatterns = [
    # Computer views
    path(
        'v1/computer/<int:pk>/',
        views.ComputerView.as_view(),
        name='computer detail'
    ),
    path(
        'v1/computer/',
        views.ComputerUploadView.as_view(),
        name='post computer'
    ),
    path(
        'v1/computers/',
        views.ComputersView.as_view(),
        name='list user computers'
    ),

    # Status views
    path(
        'v1/status/<int:pk>/',
        views.StatusView.as_view(),
        name='change status'
    ),
    path(
        'v1/status_history/<int:pk>/',
        views.StatusHistoryView.as_view(),
        name='status history'
    ),


    # Configuration views
    path(
        'v1/configuration/<int:pk>/',
        views.ConfigurationView.as_view(),
        name='edit computer configuration'
    ),
    path(
        'v1/configuration_history/<int:pk>/',
        views.ConfigurationHistoryView.as_view(),
        name='configuration history'
    ),
    path(
        'v1/configurations_list/',
        views.ListConfigurationsView.as_view(),
        name='list possible configurations'
    )

]
