from rest_framework import serializers

from . import consts, models


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Status
        fields = ['date', 'value', ]


class PatchStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Status
        fields = ['value']


class OperatingSystemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OperatingSystem
        exclude = ['id']


class CPUSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CPU
        exclude = ['id']


class RAMSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RAM
        exclude = ['id']


class GPUSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GPU
        exclude = ['id']


class StorageDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StorageDevice
        exclude = ['id']


class ConfigurationInfoSerializer(serializers.ModelSerializer):
    os = OperatingSystemSerializer(read_only=True)
    cpu = CPUSerializer(read_only=True)
    ram = RAMSerializer(read_only=True)
    gpu = GPUSerializer(read_only=True)
    storage_device = StorageDeviceSerializer(read_only=True)

    class Meta:
        model = models.Configuration
        fields = [
            'os',
            'date',
            'cpu',
            'cpu_cores',
            'ram',
            'ram_volume',
            'storage_device',
            'storage_device_volume',
            'gpu'
        ]


class ConfigurationSerializer(serializers.ModelSerializer):
    os = serializers.SlugRelatedField(
        queryset=models.OperatingSystem.objects,
        slug_field='key'
    )

    cpu = serializers.SlugRelatedField(
        queryset=models.CPU.objects,
        slug_field='key'
    )
    ram = serializers.SlugRelatedField(
        queryset=models.RAM.objects,
        slug_field='key'
    )
    gpu = serializers.SlugRelatedField(
        queryset=models.GPU.objects,
        slug_field='key'
    )
    storage_device = serializers.SlugRelatedField(
        queryset=models.StorageDevice.objects,
        slug_field='key'
    )

    class Meta:
        model = models.Configuration
        fields = [
            'os',
            'date',
            'cpu',
            'cpu_cores',
            'ram',
            'ram_volume',
            'storage_device',
            'storage_device_volume',
            'gpu'
        ]


class PatchComputerRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Computer
        fields = [
            'name',
        ]


class ComputerInfoSerializer(serializers.ModelSerializer):
    status = StatusSerializer(read_only=True)
    configuration = ConfigurationInfoSerializer(read_only=True)

    class Meta:
        model = models.Computer
        fields = '__all__'


class ListConfigurationsRequestSerializer(serializers.Serializer):
    datacenter = serializers.CharField(allow_null=True)
    os = serializers.CharField(allow_null=True)
    cpu = serializers.IntegerField(allow_null=True)
    ram = serializers.IntegerField(allow_null=True)
    disk_type = serializers.CharField(allow_null=True)
    disk_space = serializers.IntegerField(allow_null=True)
    gpu = serializers.CharField(allow_null=True)

    def validate(self, data):
        # raise serializers.ValidationError
        if 'os' in data and data['os'] not in consts.POSSIBLE_OS:
            raise serializers.ValidationError(
                "You can't select such os"
            )

        if 'gpu' in data and not models.GPU.objects.filter(key=data['gpu']).exists():
            raise serializers.ValidationError(
                "You can't select such gpu"
            )

        if 'cpu' in data and data['cpu'] not in consts.POSSIBLE_CPU:
            raise serializers.ValidationError(
                f'You can select cpu only from {consts.POSSIBLE_CPU}'
            )

        if 'ram' in data and not consts.MIN_RAM <= data['ram'] <= consts.MAX_RAM:
            raise serializers.ValidationError(
                f'You can select ram only from [{consts.MIN_RAM}, {consts.MAX_RAM}]'
            )

        if 'ram' in data and 'cpu' in data:
            if data['ram'] % data['cpu']:
                raise serializers.ValidationError(
                    'ram should be divided by cpu'
                )
            if not consts.MIN_RAM_CPU_COEF <= data['ram'] // data['cpu'] <= consts.MAX_RAM_CPU_COEF:
                raise serializers.ValidationError(
                    f'ram/cpu should be from [{consts.MIN_RAM_CPU_COEF}, {consts.MAX_RAM_CPU_COEF}]'
                )

        if 'disk_type' in data:
            if data['disk_type'] not in consts.POSSIBLE_DISK_TYPES:
                raise serializers.ValidationError(
                    f'disk type should be from {consts.POSSIBLE_DISK_TYPES}'
                )

            disk_space_fits_limits = \
                consts.MIN_DISK_SPACE[data['os']] <= data['disk_space'] and \
                data['disk_space'] <= consts.MAX_DISK_SPACE
            if 'os' in data and not disk_space_fits_limits:
                raise serializers.ValidationError(
                    'disk space should be from '
                    f"[{consts.MIN_DISK_SPACE[data['os']]}, {consts.MAX_DISK_SPACE}]"
                )

        return data


class SelectionSerializer(serializers.Serializer):
    selection_type = serializers.ChoiceField(
        choices=('range', 'choices')
    )

    min_value = serializers.IntegerField(allow_null=True)
    max_value = serializers.IntegerField(allow_null=True)

    choices = serializers.ListField(child=serializers.IntegerField(), allow_null=True)

    def validate(self, data):
        if data['selection_type'] == 'range' and \
                (data['min_value'] is None or data['max_value'] is None):
            raise serializers.ValidationError(
                'selection_type==range but one of borders is null'
            )

        if data['selection_type'] == 'choices' and data['choices'] is None:
            raise serializers.ValidationError(
                'selection_type==choices but choices field is null'
            )

        return data


class ListConfigurationsResposeSerializer(serializers.Serializer):
    datacenter = serializers.ListField(child=serializers.CharField())
    os = serializers.ListField(child=serializers.CharField())
    cpu = SelectionSerializer()
    ram = SelectionSerializer()
    disk_type = serializers.ListField(child=serializers.CharField())
    disk_space = SelectionSerializer()
    gpu = serializers.ListField(child=serializers.CharField())


class ComputerSerializer(serializers.ModelSerializer):
    configuration = ConfigurationSerializer()

    class Meta:
        model = models.Computer
        fields = [
            'name',
            'owner',
            'cloud_id',
            'configuration'
        ]
