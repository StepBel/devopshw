import json

from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from rest_framework.views import APIView

from . import consts, models, serializers


class StatusHistoryView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses={200: serializers.StatusSerializer(many=True)})
    def get(self, request, pk):
        computer = get_object_or_404(models.Computer, pk=pk, owner=request.user)
        statuses = models.Status.objects.filter(computer_id=computer.id)

        return Response(serializers.StatusSerializer(statuses, many=True).data)


class ConfigurationHistoryView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses={200: serializers.ConfigurationInfoSerializer(many=True)})
    def get(self, request, pk):
        computer = get_object_or_404(models.Computer, pk=pk, owner=request.user)
        configurations = models.Configuration.objects.filter(
            computer_id=computer.id)
        return Response(serializers.ConfigurationInfoSerializer(configurations, many=True).data)


def add_status(new_value, computer_pk, user):
    computer = get_object_or_404(models.Computer, pk=computer_pk, owner=user)

    if models.Status.objects.filter(computer_id=computer.id).latest().value != new_value:
        new_status = models.Status.objects.create(
            computer=computer,
            value=new_value
        )
        return Response(
            serializers.StatusSerializer(new_status).data,
            status=status.HTTP_201_CREATED
        )

    return Response(
        "Computer status hasn't changed since last request",
        status=status.HTTP_404_NOT_FOUND
    )


class StatusView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        request_body=serializers.PatchStatusSerializer,
        response={200: serializers.StatusSerializer}
    )
    def patch(self, request, pk):
        return add_status(request.data['value'], pk, request.user)


def _fetch_computer(pk, user):
    return Response(serializers.ComputerInfoSerializer(
        get_object_or_404(models.Computer, pk=pk, owner=user)).data)


class ComputerView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        responses={200: serializers.ComputerInfoSerializer}
    )
    def get(self, request, pk):
        return _fetch_computer(pk, request.user)

    @swagger_auto_schema(
        request_body=serializers.PatchComputerRequestSerializer,
        responses={200: serializers.ComputerInfoSerializer},
    )
    def patch(self, request, pk):
        computer = get_object_or_404(models.Computer, pk=pk, owner=request.user)
        computer_serializer = serializers.PatchComputerRequestSerializer(
            computer,
            data=request.data,
            partial=True
        )

        computer_serializer.is_valid(raise_exception=True)
        computer_serializer.save()

        return _fetch_computer(pk, request.user)

    @swagger_auto_schema(
        operation_description='Removes computer-user relation',
        responses={200: serializers.ComputerInfoSerializer},
    )
    def delete(self, request, pk):
        computer = get_object_or_404(models.Computer, pk=pk, owner=request.user)
        computer.owner = None
        computer.save()

        add_status('OFF', pk, request.user)

        response = _fetch_computer(pk, request.user)

        computer.owner = None
        computer.save()

        return response


class ConfigurationView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        request_body=serializers.ConfigurationSerializer,
        responses={200: serializers.ComputerInfoSerializer},
    )
    def patch(self, request, pk):
        computer = get_object_or_404(models.Computer, pk=pk, owner=request.user)

        configuration = models.Configuration.objects.filter(
            computer_id=computer.id).latest()
        configuration.pk = None
        configuration.date = None
        configuration.save()

        configuration_serializer = serializers.ConfigurationSerializer(
            configuration,
            data=request.data,
            partial=True
        )

        try:
            configuration_serializer.is_valid(raise_exception=True)
        except ValidationError:
            configuration.delete()
            raise

        configuration_serializer.save()

        return _fetch_computer(pk, request.user)


class ComputerUploadView(APIView):
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        operation_description='Create new computer',
        request_body=serializers.ComputerSerializer,
        responses={200: serializers.ComputerInfoSerializer},
    )
    def post(self, request):
        request_data = json.loads(request.body)

        computer = models.Computer.objects.create(
            owner=request_data['owner'],
            name=request_data.get('name'),
            cloud_id=request_data['cloud_id']
        )

        request_configuration_serializer = serializers.ConfigurationSerializer(
            data=request_data['configuration'])
        request_configuration_serializer.is_valid(raise_exception=True)

        request_configuration = request_data['configuration']
        models.Configuration.objects.create(
            computer=computer,
            os=get_object_or_404(models.OperatingSystem, key=request_configuration['os']),
            datacenter=get_object_or_404(
                models.DataCenter, key=request_configuration['datacenter']),
            cpu=get_object_or_404(models.CPU, key=request_configuration['cpu']),
            cpu_cores=int(request_configuration['cpu_cores']),
            ram=get_object_or_404(models.RAM, key=request_configuration['ram']),
            ram_volume=int(request_configuration['ram_volume']),
            storage_device=get_object_or_404(
                models.StorageDevice, key=request_configuration['storage_device']),
            storage_device_volume=int(request_configuration['ram_volume']),
            gpu=get_object_or_404(models.GPU, key=request_configuration['gpu']),
        )

        models.Status.objects.create(
            computer=computer,
            value='OFF',
        )

        return _fetch_computer(computer.id, request.user)


class ListConfigurationsView(APIView):
    @swagger_auto_schema(
        request_body=serializers.ListConfigurationsRequestSerializer,
        responses={200: serializers.ListConfigurationsResposeSerializer}
    )
    def get(self, request):
        request_serializer = serializers.ListConfigurationsRequestSerializer(
            data=request.GET.dict(),
            partial=True
        )
        request_serializer.is_valid(raise_exception=True)
        request_data = request_serializer.data

        def list_or_default(key, default):
            if request_data[key] is not None:
                return [request_data[key], ]
            else:
                return default

        def make_choices(x): return {
            'selection_type': 'choices',
            'choices': x
        }

        def make_range(min_val, max_val): return {
            'selection_type': 'range',
            'min_value': min_val,
            'max_value': max_val
        }

        def get_or_range(key, min_val, max_val):
            if request_data[key] is not None:
                return make_choices([request_data[key], ])
            else:
                return make_range(min_val, max_val)

        def get_or_choices(key, choices): return make_choices(list_or_default(key, choices))

        mock_allowed_datacenters = ['msk', 'ekb']

        default_disk_space = [min(consts.MIN_DISK_SPACE.values()), consts.MAX_DISK_SPACE]
        if request_data['os'] is not None:
            default_disk_space[0] = consts.MIN_DISK_SPACE[request_data['os']]

        response_raw = {
            'datacenter': list_or_default('datacenter', mock_allowed_datacenters),
            'os': list_or_default('os', consts.POSSIBLE_OS),
            'cpu': get_or_choices('cpu', consts.POSSIBLE_CPU),
            'ram': get_or_range('ram', consts.MIN_RAM, consts.MAX_RAM),
            'disk_type': list_or_default('disk_type', consts.POSSIBLE_DISK_TYPES),
            'disk_space': get_or_range('disk_space', *default_disk_space),
            'gpu': list_or_default('gpu', models.GPU.objects.values_list('key', flat=True))
        }

        response = serializers.ListConfigurationsResposeSerializer(data=response_raw, partial=True)
        response.is_valid(raise_exception=True)

        return Response(response.data)


class ComputersView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user_computers = models.Computer.objects.filter(owner=request.user)
        return Response(serializers.ComputerInfoSerializer(user_computers, many=True).data)
