# Generated by Django 3.2.12 on 2022-05-07 16:32

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Computer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(null=True)),
                ('cloud_id', models.TextField(unique=True)),
                ('owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CPU',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField(unique=True)),
                ('full_name', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='DataCenter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField(unique=True)),
                ('name', models.TextField()),
                ('country', models.TextField()),
                ('country_flag_key', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='GPU',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField(unique=True)),
                ('full_name', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='OperatingSystem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField(unique=True)),
                ('full_name', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='RAM',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField(unique=True)),
                ('full_name', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='StorageDevice',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField(unique=True)),
                ('full_name', models.TextField()),
                ('driver_type', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(choices=[('ON', 'Computer is turned on'), ('OFF', 'Computer is turned off'), ('TURNING_ON', 'Computer is turning on'), ('TURNING_OFF', 'Computer is turning off'), ('CHANGING_CONFIGURATION', 'Changing configuration')], default='OFF', max_length=30)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('computer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='computers.computer')),
            ],
            options={
                'ordering': ['-date'],
                'get_latest_by': 'date',
            },
        ),
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('cpu_cores', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('ram_volume', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('storage_device_volume', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('computer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='computers.computer')),
                ('cpu', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='computers.cpu', to_field='key')),
                ('datacenter', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='computers.datacenter', to_field='key')),
                ('gpu', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='computers.gpu', to_field='key')),
                ('os', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='computers.operatingsystem', to_field='key')),
                ('ram', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='computers.ram', to_field='key')),
                ('storage_device', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='computers.storagedevice', to_field='key')),
            ],
            options={
                'ordering': ['-date'],
                'get_latest_by': 'date',
            },
        ),
        migrations.AddIndex(
            model_name='status',
            index=models.Index(fields=['computer'], name='computers_s_compute_6f91d4_idx'),
        ),
        migrations.AddIndex(
            model_name='status',
            index=models.Index(fields=['computer', 'date'], name='computers_s_compute_45b5de_idx'),
        ),
        migrations.AddIndex(
            model_name='configuration',
            index=models.Index(fields=['computer'], name='computers_c_compute_361144_idx'),
        ),
        migrations.AddIndex(
            model_name='computer',
            index=models.Index(fields=['owner'], name='computers_c_owner_i_4177e2_idx'),
        ),
        migrations.AddIndex(
            model_name='computer',
            index=models.Index(fields=['cloud_id'], name='computers_c_cloud_i_8042ba_idx'),
        ),
    ]
