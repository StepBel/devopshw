from django.contrib import admin

from . import models

admin.site.register(models.Computer)
admin.site.register(models.Status)
admin.site.register(models.Configuration)
admin.site.register(models.OperatingSystem)
admin.site.register(models.CPU)
admin.site.register(models.RAM)
admin.site.register(models.StorageDevice)
