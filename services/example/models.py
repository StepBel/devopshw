from django.db import models


# https://docs.djangoproject.com/en/4.0/topics/db/models/
class Driver(models.Model):
    name = models.TextField()
    license = models.TextField()


class Car(models.Model):
    car_model = models.TextField()
    release_year = models.IntegerField()
    car_number = models.TextField()
    owner = models.ForeignKey('Driver', on_delete=models.CASCADE)
