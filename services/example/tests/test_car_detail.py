import json

from django.test import TestCase

from services.example.models import Car, Driver


class TestCarDetails(TestCase):
    def setUp(self):
        Driver.objects.create(
            name='Mishanya',
            license='ldkand123'
        )
        Driver.objects.create(
            name='Oleg',
            license='kl213mkvc'
        )

        Car.objects.create(
            car_model='Kia Rio',
            release_year=2014,
            car_number='A999UE197',
            owner_id=1
        )
        Car.objects.create(
            car_model='BMW x3',
            release_year=2016,
            car_number='Q123WE456',
            owner_id=1
        )

    def test_it_works(self):
        response = self.client.get('/api/example/v1/car/1/')
        with open('services/example/tests/static/test_car_detail/expected_response.json') as f:
            expected_response = json.load(f)
            self.assertEqual(response.data, expected_response)
