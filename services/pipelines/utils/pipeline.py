import concurrent.futures as pool
import time
from typing import List, Optional

from .. import models
from .tasks import Task

threadpool = pool.ThreadPoolExecutor(max_workers=5)


class Pipeline:
    def __init__(self, tasks: Optional[List[Task]] = None, name=''):
        self.tasks = tasks
        if self.tasks is None:
            self.tasks = []
        self.name = name

    def append(self, task):
        self.tasks.append(task)

    def clone(self):
        return Pipeline(self.tasks, self.name)

    @staticmethod
    def _run_tasks(tasks, pipeline, kwargs):
        for task in tasks:
            while True:
                try:
                    kwargs = task.run(kwargs)
                    pipeline.step += 1
                    pipeline.n_errors = 0
                    pipeline.exception_str = ''
                    pipeline.save()
                    break
                except Exception as e:
                    pipeline.n_errors += 1
                    pipeline.exception_str = str(e)
                    pipeline.save()
                    time.sleep(1)

    def run(self, kwargs):
        pipeline = models.Pipeline.objects.create(
            name=self.name,
            step=0,
            n_errors=0,
            kwargs=kwargs,
        )
        self._run_tasks(self.tasks, pipeline, kwargs)

    def rerun(self, pipeline_id):
        pipeline = models.Pipeline.objects.filter(
            id=pipeline_id,
        )
        self._run_tasks(self.tasks[pipeline.step:], pipeline, pipeline.kwargs)

    def run_detached(self, kwargs):
        threadpool.submit(self.run, kwargs)
