from abc import abstractmethod


class Task:
    """
    Для использования таски в пайплайне достаточно унаследоваться от этого класса
    и переопределить метод forward

    forward - вызывается при прямом проходе по пайплайну.
    Он использует переменные из input_kwargs и возвращает переменные в output_kwargs
    """

    def __init__(self, forward_timeout: float = None):
        """
        forward_timeout - таймаут на ожидание завершения forward, в секундах
        backward_timeout аналогично
        """
        self.forward_timeout_ = forward_timeout

    def wait_for_forward(self, kwargs):
        output_kwargs = dict()
        self.__class__.forward(kwargs, output_kwargs)
        return output_kwargs
        # Это можно будет раскомментить, когда разберусь с
        # доступом к бд из разных процессов
        # manager = multiprocessing.Manager()
        # output_kwargs = manager.dict()

        # p = multiprocessing.Process(
        #     target=self.__class__.forward,
        #     args=(kwargs, output_kwargs)
        # )

        # p.start()
        # p.join(self.forward_timeout_)

        # if p.is_alive():
        #     print("Task is running too long")
        #     p.kill()
        #     p.join()
        #     raise TimeoutError

        # return output_kwargs

    @staticmethod
    @abstractmethod
    def forward(input_kwargs: dict, output_kwargs: dict):
        pass


class SetStatusTurningOn(Task):
    @staticmethod
    def run(input_kwargs: dict):
        pass


class TurnOnYCloud(Task):
    def forward(input_kwargs: dict, output_kwargs: dict):
        # Включаем комп в облаке
        pass


class SetStatusOn(Task):
    @staticmethod
    def run(input_kwargs: dict):
        pass


class SetStatusTurningOff(Task):
    @staticmethod
    def run(input_kwargs: dict):
        pass


class UnmountFileSystem(Task):
    def forward(input_kwargs: dict, output_kwargs: dict):
        # Отмаунтить файловую систему
        pass


class TurnOffYCloud(Task):
    def forward(input_kwargs: dict, output_kwargs: dict):
        # Выключить комп в облаке
        pass


class SetStatusOff(Task):
    @staticmethod
    def run(input_kwargs: dict):
        pass


class CreateYCloudComputer(Task):
    def forward(input_kwargs: dict, output_kwargs: dict):
        # Создать комп в облаке
        pass


class SetStatusChangingConfiguration(Task):
    @staticmethod
    def run(input_kwargs: dict):
        pass


class ChangeYCloudConfiguration(Task):
    def forward(input_kwargs: dict, output_kwargs: dict):
        # Меняем конфигурацию компа в облаке
        pass


class SaveNewConfiguration(Task):
    def forward(input_kwargs: dict, output_kwargs: dict):
        # Сохраняем новую конфигурацию в бд
        pass
