from django.db import models


class Pipeline(models.Model):
    name = models.TextField()
    step = models.IntegerField()
    n_errors = models.IntegerField()
    exception_str = models.TextField()
    kwargs = models.JSONField()
