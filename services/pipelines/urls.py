from django.urls import path

from . import views

urlpatterns = [
    path(
        'v1/pipeline_execution/',
        views.pipeline_execution,
        name='execute pipeline'
    ),
    path(
        'v1/pipeline_recovery/<int:pk>',
        views.pipeline_recovery,
        name='recovery pipeline'
    )
]
