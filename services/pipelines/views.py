from rest_framework.decorators import api_view
from rest_framework.response import Response


# @swagger_auto_schema(methods=['post'],
# request_body=serializers.PipelineExecutionRequestSerializer)
@api_view(['POST'])
def pipeline_execution(request):
    return Response()


# @swagger_auto_schema(methods=['post'])
@api_view(['POST'])
def pipeline_recovery(request, pk):
    return Response()
